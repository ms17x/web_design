Web Design Projects

Author: Mitchnikov Seide

Click blue links blow for projects:


[A1 README.md](a1/README.md "My A1 README.md file")

- Created a redesign of Carols Critters
- Provided screenshots 

[A2 README.md](a2/README.md "My A2 README.md file")

- Art gallery webpage
- Provided screenshots 

[A3 README.md](a3/README.md "My A3 README.md file")

- My portfolio web design
- Provided screenshots

[A4 README.md](a4/README.md "My A4 README.md file")

- My redesign of the converse website
- Provided screenshot

[A5 README.md](a5/README.md "My A5 README.md file")

- My redesign of the American airlines website
- Provided screenshots

[A6 README.md](a6/README.md "My A6 README.md file")

- My redesign of the LeasetoOwn website
- Provided screenshots

[A7 README.md](a7/README.md "My A7 README.md file")

- My design of a portfolio website
- Provided screenshots